/*
 * --------------------------------------------------------------------------------------------------------------------
 * Example sketch/program showing how to read data from a PICC to serial.
 * --------------------------------------------------------------------------------------------------------------------
 * This is a MFRC522 library example; for further details and other examples see: https://github.com/miguelbalboa/rfid
 *
 * Example sketch/program showing how to read data from a PICC (that is: a RFID Tag or Card) using a MFRC522 based RFID
 * Reader on the Arduino SPI interface.
 *
 * When the Arduino and the MFRC522 module are connected (see the pin layout below), load this sketch into Arduino IDE
 * then verify/compile and upload it. To see the output: use Tools, Serial Monitor of the IDE (hit Ctrl+Shft+M). When
 * you present a PICC (that is: a RFID Tag or Card) at reading distance of the MFRC522 Reader/PCD, the serial output
 * will show the ID/UID, type and any data blocks it can read. Note: you may see "Timeout in communication" messages
 * when removing the PICC from reading distance too early.
 *
 * If your reader supports it, this sketch/program will read all the PICCs presented (that is: multiple tag reading).
 * So if you stack two or more PICCs on top of each other and present them to the reader, it will first output all
 * details of the first and then the next PICC. Note that this may take some time as all data blocks are dumped, so
 * keep the PICCs at reading distance until complete.
 *
 * @license Released into the public domain.
 *
 */

#include <MFRC522.h>
#include <esp_log.h>
static const char LOG_TAG[] = "DumpInfo";
#define SS_PIN          15
#define RST_PIN         2
MFRC522 mfrc522;  // Create MFRC522 instance


void dumpInfo() {
	MFRC522::StatusCode status;
	MFRC522::MIFARE_Key key;
	byte byteCount;
	byte buffer[18];
	byteCount = sizeof(buffer);
	for (byte i = 0; i < 6; i++) {
		key.keyByte[i] = 0xFF;
	}


    byte dataBlock[]    = {
        0x01, 0x02, 0x03, 0x04, //  1,  2,   3,  4,
        0x05, 0x06, 0x07, 0x08, //  5,  6,   7,  8,
        0x08, 0x09, 0xff, 0x0b, //  9, 10, 255, 12,
        0x0c, 0x0d, 0x0e, 0x0f  // 13, 14,  15, 16
    };
    byte dataBlockSize = sizeof(dataBlock);
	mfrc522.PCD_Init(SS_PIN, RST_PIN);		// Init MFRC522
	ESP_LOGI(LOG_TAG, "init OK");
	mfrc522.PCD_DumpVersionToSerial();	// Show details of PCD - MFRC522 Card Reader details
	ESP_LOGD(LOG_TAG, "Scan PICC to see UID, SAK, type, and data blocks...");
	while(1) {

		// Look for new cards
		if ( ! mfrc522.PICC_IsNewCardPresent()) {
			continue;
		}

		// Select one of the cards
		if ( ! mfrc522.PICC_ReadCardSerial()) {
			continue;
		}
//        mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
//		// Dump debug info about the card; PICC_HaltA() is automatically called
		status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 1, &key,&(mfrc522.uid));
					if (status != MFRC522::STATUS_OK) {
						ESP_LOGD(LOG_TAG, "FAIL");
						continue;}
					// Read block
		byteCount = sizeof(buffer);
//		status =mfrc522.MIFARE_Read(1, buffer, &byteCount);
		mfrc522.MIFARE_Write(1,dataBlock,dataBlockSize);

		if (status != MFRC522::STATUS_OK) {
			ESP_LOGD(LOG_TAG, "erro");
						continue;
					}
		status =mfrc522.MIFARE_Read(1, buffer, &byteCount);
		if (status != MFRC522::STATUS_OK) {
					ESP_LOGD(LOG_TAG, "FAIL eerrro");
								continue;
							}
		for (byte i = 0; i < 16; i++) {
			ESP_LOGI(LOG_TAG, "%x",buffer[i]);
		}


		mfrc522.PICC_HaltA();
		mfrc522.PCD_StopCrypto1();
	}
}
