#include <stdio.h>
#include <Task.h>
extern void dumpInfo();


#include <MFRC522.h>
#include <esp_log.h>
static const char LOG_TAG[] = "DumpInfo";
#define SS_PIN          15
#define RST_PIN         2
extern "C" {
	void app_main();
}

class MyTask: public Task {
	void run(void* data) {
		printf("Hello world!");
		dumpInfo();
		printf("Done\n");
	}
};



void app_main() {
	MyTask* pMyTask = new MyTask();
	pMyTask->setStackSize(20000);
	pMyTask->start();
	}


